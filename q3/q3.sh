#!/bin/bash

vida_usuario=1
vida_dragao=2

exibir_dragao() {
    cowsay -f dragon "Dragão"
}

exibir_vida() {
    echo "Sua vida: $vida_usuario"
    echo "Vida do Dragão: $vida_dragao"
}

atacar_dragao() {
    vida_dragao=$((vida_dragao - 100))
    exibir_vida
}

fugir() {
    resultado=$((RANDOM % 2))  # 0 - Derrota | 1 - Sucesso

    if [ $resultado -eq 0 ]; then
        echo "Você foi derrotado pela baforada do Dragão!"
        vida_usuario=0
    else
        echo "Você conseguiu escapar do Dragão!"
    fi
}

# Loop principal
while [ $vida_usuario -gt 0 ] && [ $vida_dragao -gt 0 ]; do
    clear
    exibir_dragao
    echo
    exibir_vida

    echo "Opções:"
    echo "1 - Atacar o Dragão"
    echo "2 - Fugir"

    read -p "Escolha uma opção: " opcao

    case $opcao in
        1)
            atacar_dragao
            ;;
        2)
            fugir
            ;;

    esac

    read -p "Pressione Enter para continuar..."
done

if [ $vida_usuario -eq 0 ]; then
    echo "Você foi derrotado pelo Dragão!"
else
    echo "Parabéns! Você derrotou o Dragão!"
fi

