#!/bin/bash

lista_arquivo="lista.txt"

while IFS= read -r arquivo; do
    if [ -f "$arquivo" ]; then
        md5sum "$arquivo"
	md5sum "$arquivo" >> resultado.txt
    else
        echo "Arquivo não encontrado: $arquivo"
    fi
done < "$lista_arquivo"

