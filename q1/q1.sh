#!/bin/bash

exibir_menu() {
    echo "Menu:"
    echo "1 -> Verificar se o usuário existe"
    echo "2 -> Verificar se o usuário está logado"
    echo "3 -> Listar os arquivos da pasta home do usuário"
    echo "4 -> Sair"
}

verificar_existencia_usuario() {
    read -p "Digite o nome de usuário a ser verificado: " username
    id -u $username >/dev/null 2>&1
    if [ $? -eq 0 ]; then	
        echo "O usuário $username existe."
    else
        echo "O usuário $username não existe."
		
    fi
}

verificar_usuario_logado() {
    read -p "Digite o nome de usuário a ser verificado: " username
    who | grep -wq $username
    if [ $? -eq 0 ]; then
        echo "O usuário $username está logado."
    else
        echo "O usuário $username não está logado."
    fi
}

listar_arquivos_home_usuario() {
    read -p "Digite o nome de usuário para listar os arquivos da pasta home: " username
    home_dir=$(eval echo "~$username")
    if [ -d $home_dir ]; then
        echo "Arquivos na pasta home do usuário $username:"
        ls -la $home_dir
    else
        echo "O usuário $username não possui uma pasta home válida."
	break
    fi
}

# Loop principal


while true; do
    clear
    exibir_menu
    read -p "Escolha uma opção: " opcao

    case $opcao in
        1)
            verificar_existencia_usuario
            ;;
        2)
            verificar_usuario_logado
            ;;
        3)
            listar_arquivos_home_usuario
            ;;
        4)
            break
            ;;
	d)
            break
            ;;
	
       
    esac

    echo  # Linha em branco para separar as saídas

    read -p "Pressione Enter para continuar..."
done

